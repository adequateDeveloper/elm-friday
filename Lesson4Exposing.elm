module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)


-- Sorts a list of names and renders them as an HTML list


names =
    [ "Pearl"
    , "Steven"
    , "Garnet"
    , "Amethyst"
    ]


textToItem text =
    li
        [ style [ ( "font-style", "italic" ) ] ]
        [ text ]


main =
    names
        |> List.sort
        |> List.map Html.text
        |> List.map textToItem
        |> ul []
