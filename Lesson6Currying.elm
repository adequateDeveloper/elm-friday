module Main exposing (..)

import Html


multiply : number -> number -> number
multiply a b =
    a * b


multiplyByFive : number -> number
multiplyByFive =
    multiply 5



-- The expression (multiply 5) yields a new function with signature
-- (number -> number) by partial application, that is: the first argument to
-- multiply is provided, but not the second.


main : Html.Html msg
main =
    multiplyByFive 3 |> toString |> Html.text



-- As you probably have guessed, the output of this snippet is 15.
