module Main exposing (..)

import Html


names =
    [ "Pearl", "Steven", "Garnet", "Amethyst" ]


textToItem text =
    Html.li [] [ text ]


main =
    names
        |> List.sort
        |> List.map Html.text
        |> List.map textToItem
        |> Html.ul []
