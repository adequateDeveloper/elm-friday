
Elm Friday Tutorials
--------------------
https://blog.codecentric.de/en/2015/11/elm-friday-table-of-contents/

http://elm-lang.org/docs/style-guide

http://package.elm-lang.org/packages/circuithub/elm-list-extra/latest/List-Extra
-------------------------------------------------------------------------------
elm repl    (multi-line: end each line with \ and start subsequent lines with a space)
elm reactor
elm format
elm make --warn Functions.elm

-------------------------------------------------------------------------------


Elm does not render strings to HTML directly, you always have to turn strings into HTML text elements first.

Nearly all functions from the Html module that yield an HTML tag take two arguments: a list of attributes and a list of
inner elements. The first argument (the attributes) can be used to set a CSS class, inline styles, event listeners or
anything else that is represented as an attribute in HTML. The second argument, the inner element list is the list of
HTML elements that will be wrapped in the new HTML element.

elm-friday-part-05-functions/
-----------------------------
Equivalent functions:

    > incAll list = List.map (\ n -> n + 1) list
    <function> : List number -> List number
    > incAll [1, 2, 3]
    [2,3,4] : List number

    > incAll2 = List.map (\ n -> n + 1)
    <function> : List number -> List number
    > incAll2 [1, 2, 3]
    [2,3,4] : List number

    As a rule of thumb, if the last element in the parameter list in the function declaration is simply repeated at the
    end of the function body (like list in this case), you can probably omit both.

Elm has two operators, |> and <|, to write expressions in a more elegant fashion.

|>: Take the expression to the left of the operator and put it into the function on the right hand side.
<|: Take the expression to the right of the operator and put it into the function on the left hand side.


